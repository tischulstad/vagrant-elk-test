#!/usr/bin/env bash

# Set some variables
PASSWORD='vagrant'
KIBANA_ADMIN_USER='kibanaadmin'

export DEBIAN_FRONTEND=noninteractive

# Logstash and Elasticsearch requires java
#sudo add-apt-repository -y ppa:webupd8team/java
sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt-get -y install openjdk-8-jdk
#sudo apt-get -y install oracle-java8-installer

#
# Elasticsearch
# 

# Install
wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb http://packages.elastic.co/elasticsearch/2.x/debian stable main" | sudo tee -a /etc/apt/sources.list.d/elasticsearch-2.x.list
sudo apt-get update
sudo apt-get -y install elasticsearch

#Configure Elasticsearch and restart service
ESCONF=$(cat <<EOF
network.host: localhost
EOF
)
sudo mv /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch_orig.yml
sudo echo "${ESCONF}" > /etc/elasticsearch/elasticsearch.yml
sudo service elasticsearch restart

# Start Elasticsearch at boot
sudo update-rc.d elasticsearch defaults 95 10


#
# Kibana
#

# Install
echo "deb http://packages.elastic.co/kibana/4.4/debian stable main" | sudo tee -a /etc/apt/sources.list.d/kibana-4.4.x.list
sudo apt-get update
sudo apt-get -y install kibana

# Configure Kibana, start at boot and restart
KIBCONF=$(cat <<EOF
server.host: "localhost"
EOF
)
sudo mv /opt/kibana/config/kibana.yml /opt/kibana/config/kibana_orig.yml
sudo echo "${KIBCONF}" > /opt/kibana/config/kibana.yml
sudo update-rc.d kibana defaults 96 9
sudo service kibana restart


#
# Install nginx and configure as reverse proxy
#
sudo apt-get -y install nginx apache2-utils
sudo htpasswd -cb /etc/nginx/htpasswd.users ${KIBANA_ADMIN_USER} ${PASSWORD}
NGXCONF=$(cat <<EOF
server {
    listen 80;

    server_name logserver.dev;

    auth_basic "Restricted Access";
    auth_basic_user_file /etc/nginx/htpasswd.users;

    location / {
        proxy_pass http://localhost:5601;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;        
    }
}
EOF
)
sudo sh -c "echo '${NGXCONF}' > /etc/nginx/sites-available/default"
sudo service nginx restart

# 
# Logstash
#

# Install
echo 'deb http://packages.elastic.co/logstash/2.2/debian stable main' | sudo tee /etc/apt/sources.list.d/logstash-2.2.x.list
sudo apt-get update
sudo apt-get install logstash

# genetate ssl certificates
sudo mkdir -p /etc/pki/tls/certs
sudo mkdir /etc/pki/tls/private
sudo sed -i '/\[ v3_ca \]/a subjectAltName = IP: 192.168.33.20' /etc/ssl/openssl.cnf #Needed since we have no fqdn(dns)

cd /etc/pki/tls
sudo openssl req -config /etc/ssl/openssl.cnf -x509 -days 3650 -batch -nodes -newkey rsa:2048 -keyout private/logstash-forwarder.key -out certs/logstash-forwarder.crt

# Configure Logstash

BEATSINPUT=$(cat <<EOF
input {
  beats {
    port => 5044
    ssl => true
    ssl_certificate => "/etc/pki/tls/certs/logstash-forwarder.crt"
    ssl_key => "/etc/pki/tls/private/logstash-forwarder.key"
  }
}
EOF
)
sudo sh -c "echo '${BEATSINPUT}' > /etc/logstash/conf.d/02-beats-input.conf"

SYSLOGFILTER=$(cat <<EOF
filter {
  if [type] == "syslog" {
    grok {
      match => { "message" => "%{SYSLOGTIMESTAMP:syslog_timestamp} %{SYSLOGHOST:syslog_hostname} %{DATA:syslog_program}(?:\[%{POSINT:syslog_pid}\])?: %{GREEDYDATA:syslog_message}" }
      add_field => [ "received_at", "%{@timestamp}" ]
      add_field => [ "received_from", "%{host}" ]
    }
    syslog_pri { }
    date {
      match => [ "syslog_timestamp", "MMM  d HH:mm:ss", "MMM dd HH:mm:ss" ]
    }
  }
}
EOF
)
sudo sh -c "echo '${SYSLOGFILTER}' > /etc/logstash/conf.d/10-syslog-filter.conf"

ESOUTPUT=$(cat <<EOF
output {
  elasticsearch {
    hosts => ["localhost:9200"]
    sniffing => true
    manage_template => false
    index => "%{[@metadata][beat]}-%{+YYYY.MM.dd}"
    document_type => "%{[@metadata][type]}"
  }
}
EOF
)
sudo sh -c "echo '${ESOUTPUT}' > /etc/logstash/conf.d/30-elasticsearch-output.conf"

sudo service logstash restart
sudo update-rc.d logstash defaults 96 9

# Download Elastics sample Kibana dashboards archive
cd ~
curl -L -O https://download.elastic.co/beats/dashboards/beats-dashboards-1.1.0.zip
sudo apt-get -y install unzip
unzip beats-dashboards-*.zip
cd beats-dashboards-*
./load.sh

# Download and load the Filebeat index template
cd ~
curl -O https://gist.githubusercontent.com/thisismitch/3429023e8438cc25b86c/raw/d8c479e2a1adcea8b1fe86570e42abab0f10f364/filebeat-index-template.json
curl -XPUT 'http://localhost:9200/_template/filebeat?pretty' -d@filebeat-index-template.json

#
# The logserver should now be ready to receive Filebeat data 
#


# Add Apache filter to logstash 
APACHEFILTER=$(cat <<EOF
filter {
  if [type] == "apache-access" {
    grok {
      match => { "message" => "%{COMBINEDAPACHELOG}" }
    }
  }
}
EOF
)
sudo sh -c "echo '${APACHEFILTER}' > /etc/logstash/conf.d/12-apache.conf"
sudo service logstash restart