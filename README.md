# ELK testing in Vagrant

This is my playground for testing out the ELK logging stack. ELK is **Elasticsearch**, **Logstash** and **Kibana**

Vagrant up will provision two servers

* logserver: Logserver that aggregate and present log data
    * Kibana available at http://192.168.33.20/
    * Username: kibanaadmin 
    * Password: vagrant
* webserver: LAMP stac that various log data to logserver
    * Runs a wordpress installation. Available at http://192.168.33.30/
    * Ships syslog and auth.log to logserver with the filebeat shipper 

Much of the setup is inspired from the tutorials starting at this page:
https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elk-stack-on-ubuntu-14-04