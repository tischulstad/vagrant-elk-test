#!/usr/bin/env bash

# Set some variables
PASSWORD='vagrant'
PROJECTFOLDER='testproject'
LOGSERVERIP='192.168.33.20'

export DEBIAN_FRONTEND=noninteractive

# create project folder
sudo mkdir -p /var/www/html/${PROJECTFOLDER}

# update / upgrade
sudo apt-get update
sudo apt-get -y upgrade

# install apache  and php
sudo apt-get install -y apache2
sudo apt-get install -y php5

# install mysql and give password to installer
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
sudo apt-get -y install mysql-server
sudo apt-get install php5-mysql

# install phpmyadmin and give password(s) to installer
# for simplicity I'm using the same password for mysql and phpmyadmin
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt-get -y install phpmyadmin

# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    ServerName ${PROJECTFOLDER}.dev
    DocumentRoot "/var/www/html/${PROJECTFOLDER}"
    <Directory "/var/www/html/${PROJECTFOLDER}">
        Options -Indexes +FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

# enable mod_rewrite
sudo a2enmod rewrite

# restart apache
service apache2 restart

# install git
sudo apt-get -y install git

# install Composer
curl -s https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

#
# Set up a vanilla Wordpress to have something to generate test data...
#

# Download and install wp-cli
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

#Install WP
cd /var/www/html/${PROJECTFOLDER}
wp core download --allow-root --locale=nb_NO
wp core config --dbuser=root --dbpass=${PASSWORD} --dbname=${PROJECTFOLDER} --allow-root
wp db create --allow-root
wp core install --url=${PROJECTFOLDER}.dev --title=Testinstallasjon --admin_user=admin --admin_password=${PASSWORD} --admin_email=tor.inge.schulstad@telemed.no --allow-root --skip-email


#
# Get ssl-certificate from logserver and install Filebeat package to ship logs to logserver
#
cd ~
sudo apt-get -y install sshpass
sshpass -p "vagrant" scp -o StrictHostKeyChecking=no -r vagrant@192.168.33.20:/etc/pki/tls/certs/logstash-forwarder.crt /home/vagrant
sudo mkdir -p /etc/pki/tls/certs
sudo cp /home/vagrant/logstash-forwarder.crt /etc/pki/tls/certs/

echo "deb https://packages.elastic.co/beats/apt stable main" |  sudo tee -a /etc/apt/sources.list.d/beats.list
wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install filebeat

sudo mv /etc/filebeat/filebeat.yml /etc/filebeat/filebeat.yml.orig
sudo mv /home/vagrant/filebeat.yml /etc/filebeat/filebeat.yml
sudo service filebeat restart
sudo update-rc.d filebeat defaults 95 10